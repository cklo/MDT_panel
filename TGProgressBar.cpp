#include <TGClient.h>
#include <TGFrame.h>
#include <TGProgressBar.h>
#include <TApplication.h>

int main(int argc, char** argv)
{
    TApplication app("app", &argc, argv);

    // Create the main frame
    TGMainFrame* fMain = new TGMainFrame(gClient->GetRoot(), 400, 300);

    // Create the vertical frame
    TGVerticalFrame* vframe = new TGVerticalFrame(fMain, 10, 10);

    // Create the progress bars
    TGHProgressBar* fHProg1 = new TGHProgressBar(vframe, TGProgressBar::kStandard, 300);
    fHProg1->ShowPosition();
    fHProg1->SetBarColor("yellow");
    fHProg1->SetPosition(90.0);

    TGHProgressBar* fHProg2 = new TGHProgressBar(vframe, TGProgressBar::kFancy, 300);
    fHProg2->SetBarColor("lightblue");
    fHProg2->ShowPosition(kTRUE, kFALSE, "%.0f events");
    fHProg2->SetPosition(45.0);

    TGHProgressBar* fHProg3 = new TGHProgressBar(vframe, TGProgressBar::kStandard, 300);
    fHProg3->SetFillType(TGProgressBar::kBlockFill);
    fHProg3->SetPosition(45.0);

    // Add the progress bars to the vertical frame
    vframe->AddFrame(fHProg1, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 10));
    vframe->AddFrame(fHProg2, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 10));
    vframe->AddFrame(fHProg3, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 10));

    // Add the vertical frame to the main frame
    fMain->AddFrame(vframe, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

    // Map the main frame and show it
    fMain->MapSubwindows();
    fMain->Resize(fMain->GetDefaultSize());
    fMain->MapWindow();

    // Run the application event loop
    app.Run();

    return 0;
}