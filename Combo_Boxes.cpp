#include <TGClient.h>
#include <TGFrame.h>
#include <TApplication.h>
#include <TGComboBox.h>

int main(int argc, char** argv)
{
    TApplication app("app", &argc, argv);

    // Create the main frame
    TGMainFrame* fMain = new TGMainFrame(gClient->GetRoot(), 400, 300);

    // Create the parent frame for the combo box
    TGCompositeFrame* parent = new TGCompositeFrame(fMain, 10, 10, kHorizontalFrame);

    // Combo box layout hints
    TGLayoutHints* fLcombo = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5);

    // Combo box widget
    TGComboBox* fCombo = new TGComboBox(parent, 100);
    char tmp[20];
    for (int i = 0; i < 4; i++) {
        sprintf(tmp, "Entry%i", i + 1);
        fCombo->AddEntry(tmp, i + 1);
    }
    fCombo->Resize(150, 20);
    fCombo->Select(2); // Entry3 is selected as current

    // Add the combo box to the parent frame
    parent->AddFrame(fCombo, fLcombo);

    // Add the parent frame to the main frame
    fMain->AddFrame(parent, new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 5, 5, 5, 5));

    // Map the main frame and show it
    fMain->MapSubwindows();
    fMain->Resize(fMain->GetDefaultSize());
    fMain->MapWindow();

    // Run the application event loop
    app.Run();

    return 0;
}