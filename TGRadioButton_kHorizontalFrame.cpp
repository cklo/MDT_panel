#include <iostream>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TApplication.h>

void ChangeCoordinateSystem(int id)
{
    // Handle the coordinate system change event
    switch (id) {
        case 0:
            // Pixel coordinate system
            std::cout << "Pixel coordinate system selected" << std::endl;
            break;
        case 1:
            // NDC coordinate system
            std::cout << "NDC coordinate system selected" << std::endl;
            break;
        case 2:
            // User coordinate system
            std::cout << "User coordinate system selected" << std::endl;
            break;
        default:
            break;
    }
}

int main(int argc, char** argv)
{
    TApplication app("app", &argc, argv);

    // Create the main frame
    TGMainFrame* mainFrame = new TGMainFrame(gClient->GetRoot(), 400, 200);

    // Create the button group frame
    TGButtonGroup* br = new TGButtonGroup(mainFrame, "Coordinate system", kHorizontalFrame);

    // Create the radio buttons
    TGRadioButton* fR[3];
    fR[0] = new TGRadioButton(br, "&Pixel", 0);
    fR[1] = new TGRadioButton(br, "&NDC", 1);
    fR[2] = new TGRadioButton(br, "&User", 2);
    fR[1]->SetState(kButtonDown);

    // Connect the radio buttons to the slot function
    for (int i = 0; i < 3; ++i) {
        fR[i]->Connect("Clicked()", "ChangeCoordinateSystem(int)", nullptr, Form("%d", i));
    }

    // Add the button group to the main frame
    mainFrame->AddFrame(br, new TGLayoutHints(kLHintsCenterX, 10, 10, 10, 10));

    // Map the main frame and show it
    mainFrame->MapSubwindows();
    mainFrame->Resize(mainFrame->GetDefaultSize());
    mainFrame->MapWindow();

    // Run the application event loop
    app.Run();

    return 0;
}